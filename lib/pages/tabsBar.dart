/*
 * @Author: your name
 * @Date: 2021-11-22 15:25:16
 * @LastEditTime: 2022-01-27 10:37:22
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \flutter_application_1\lib\main.dart
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TabsBar extends StatefulWidget {
  @override
  _TabViewRoute1State createState() => _TabViewRoute1State();
}

class _TabViewRoute1State extends State<TabsBar>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  List tabs = ["新闻", "头条", "消息"];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: tabs.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("App Name"),
        bottom: TabBar(
          controller: _tabController,
          tabs: tabs.map((e) => Tab(text: e)).toList(),
        ),
      ),
      body: TabBarView(
        //构建
        controller: _tabController,
        children: tabs.map((e) {
          print(_tabController.toString());
          return Container(
            alignment: Alignment.center,
            child: new ListPage(),
            // child: Text('123123'),

            width: double.infinity,
            height: double.infinity,
          );
        }).toList(),
      ),
    );
  }

  @override
  void dispose() {
    // 释放资源
    _tabController.dispose();
    super.dispose();
  }
}

// 列表页
class ListPage extends StatefulWidget {
  ListPage({Key? key}) : super(key: key);

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  List<String> list = [
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
    '12',
  ];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // for(var i=0;i<100;i++){
    //   list.push(i);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: list.map((c) => CarItems).toList(),
      ),
    );
  }
}

Widget CarItems = Container(
    margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(
        Radius.circular(4.0),
      ),
    ),
    child: Flex(
      direction: Axis.horizontal,
      children: <Widget>[
        Container(
          width: 60.0,
          height: 60,
          margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
          child: Image.network(
            "https://avatars2.githubusercontent.com/u/20411648?s=460&v=4",
            width: 60.0,
            height: 60.0,
          ),
        ),
        Expanded(
          flex: 1,
          child: Column(
            children: [
              Text(
                '标题标题标题标题',
                textScaleFactor: 2.0,
              )
            ],
          ),
        ),
      ],
    ));
