/*
 * @Author: your name
 * @Date: 2021-11-22 15:25:16
 * @LastEditTime: 2022-01-21 16:57:24
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \flutter_application_1\lib\main.dart
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Demos extends StatelessWidget {
  Demos({
    Key? key,
    @required id,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var args=ModalRoute.of(context).settings.arguments;
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('学习demos'), //标题
        elevation: 10.0, //下阴影
        centerTitle: true, //居中文字
      ),
      body: new SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(15),
            child: Column(
              //测试Row对齐方式，排除Column默认居中对齐的干扰
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                    height: 44,
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                    decoration: BoxDecoration(
                      color: Colors.cyan[100],
                      borderRadius: BorderRadius.all(
                        Radius.circular(4.0),
                      ),
                    ),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, '/layout', arguments: {});
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("布局组件"),
                        ],
                      ),
                    )),
                Container(
                    height: 44,
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                    decoration: BoxDecoration(
                      color: Colors.cyan[200],
                      borderRadius: BorderRadius.all(
                        Radius.circular(4.0),
                      ),
                    ),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, '/contain', arguments: {});
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("容器组件"),
                        ],
                      ),
                    )),
                Container(
                    height: 44,
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                    decoration: BoxDecoration(
                      color: Colors.cyan[300],
                      borderRadius: BorderRadius.all(
                        Radius.circular(4.0),
                      ),
                    ),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, '/scroll', arguments: {});
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("可滚动组件"),
                        ],
                      ),
                    ))
              ],
            ),
          ),
          scrollDirection: Axis.vertical),
    );
  }
}
