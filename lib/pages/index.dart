/*
 * @Author: your name
 * @Date: 2021-11-24 14:34:17
 * @LastEditTime: 2022-01-20 15:06:57
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \flutter_application_1\lib\views\page1.dart
 */

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './tabsBar.dart';
// import './layout.dart';
import './demos.dart';

class Index extends StatefulWidget {
  Index({Key? key}) : super(key: key);
  List<Widget> coms = [TabsBar(), Demos()];
  @override
  _nameState createState() => _nameState();
}

class _nameState extends State<Index> {
  int _index = 0;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      // appBar: new AppBar(
      //   title: new Text('首页'), //标题
      //   elevation: 10.0, //下阴影
      //   centerTitle: true, //居中文字
      // ),
      body: widget.coms[_index],
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: '首页'),
          BottomNavigationBarItem(icon: Icon(Icons.add), label: '我的')
        ],
        currentIndex: _index,
        onTap: (v) {
          setState(() {
            _index = v;
          });
        },
      ),
    );
  }
}
