/*
 * @Author: your name
 * @Date: 2021-11-22 15:25:16
 * @LastEditTime: 2022-01-21 16:55:05
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \flutter_application_1\lib\main.dart
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Widget redBox = DecoratedBox(
//   decoration: BoxDecoration(color: Colors.red),
// );
// Widget avatar = FlutterLogo(
//   size: 60,
// );

class Scroll extends StatelessWidget {
  Scroll({
    Key? key,
    @required id,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var args=ModalRoute.of(context).settings.arguments;\
    String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('滚动组件'), //标题
          elevation: 10.0, //下阴影
          centerTitle: true, //居中文字
          actions: <Widget>[
            //导航栏右侧菜单
            IconButton(icon: Icon(Icons.share), onPressed: () {}),
          ],
        ),
        body: Scrollbar(
          // 显示进度条
          child: SingleChildScrollView(
            padding: EdgeInsets.all(16.0),
            child: Center(
              child: Column(
                //动态创建一个List<Widget>
                children: str
                    .split("")
                    //每一个字母都用一个Text显示,字体为原来的两倍
                    .map((c) => Text(
                          c,
                          textScaleFactor: 2.0,
                        ))
                    .toList(),
              ),
            ),
          ),
        ));
  }
}
