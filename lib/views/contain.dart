/*
 * @Author: your name
 * @Date: 2021-11-22 15:25:16
 * @LastEditTime: 2022-01-21 16:49:54
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \flutter_application_1\lib\main.dart
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget redBox = DecoratedBox(
  decoration: BoxDecoration(color: Colors.red),
);
Widget avatar = FlutterLogo(
  size: 60,
);

class Contain extends StatelessWidget {
  Contain({
    Key? key,
    @required id,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var args=ModalRoute.of(context).settings.arguments;
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('容器组件'), //标题
          elevation: 10.0, //下阴影
          centerTitle: true, //居中文字
          actions: <Widget>[
            //导航栏右侧菜单
            IconButton(icon: Icon(Icons.share), onPressed: () {}),
          ],
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: BottomAppBar(
          color: Colors.white,
          shape: CircularNotchedRectangle(), // 底部导航栏打一个圆形的洞
          child: Row(
            children: [
              Text('底部1'),
              SizedBox(), //中间位置空出
              Text('底部2'),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceAround, //均分底部导航栏横向空间
          ),
        ),
        body: new Column(children: [
          Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              // fit: StackFit.expand, //未定位widget占满Stack整个空间
              children: <Widget>[
                ConstrainedBox(
                  constraints: BoxConstraints(
                      minWidth: double.infinity, //宽度尽可能大
                      minHeight: 50.0 //最小高度为50像素
                      ),
                  child: Container(
                    height: 5.0,
                    child: redBox,
                  ),
                ),
                SizedBox(width: 80.0, height: 80.0, child: redBox),
                ConstrainedBox(
                    constraints:
                        BoxConstraints(minWidth: 60.0, minHeight: 100.0), //父
                    child: UnconstrainedBox(
                      //“去除”父级限制
                      child: ConstrainedBox(
                        constraints:
                            BoxConstraints(minWidth: 90.0, minHeight: 20.0), //子
                        child: redBox,
                      ),
                    )),
                Container(
                  color: Colors.black,
                  child: Transform(
                    alignment: Alignment.topRight, //相对于坐标系原点的对齐方式
                    transform: Matrix4.skewX(1), //沿Y轴倾斜0.3弧度
                    child: Container(
                      padding: const EdgeInsets.all(8.0),
                      color: Colors.deepOrange,
                      child: const Text('Apartment for rent!'),
                    ),
                  ),
                ),
                ClipOval(child: avatar), //剪裁为圆形
                ClipRRect(
                  //剪裁为圆角矩形
                  borderRadius: BorderRadius.circular(5.0),
                  child: avatar,
                ),
                Container(
                  width: 50,
                  height: 50,
                  color: Colors.red,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    // 子容器超过父容器大小
                    child: Container(width: 30, height: 70, color: Colors.blue),
                  ),
                ),
              ],
            ),
          ),
        ]));
  }
}
