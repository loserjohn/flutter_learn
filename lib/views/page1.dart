/*
 * @Author: your name
 * @Date: 2021-11-24 14:34:17
 * @LastEditTime: 2022-01-20 16:00:26
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \flutter_application_1\lib\views\page1.dart
 */

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'user.dart';

class SwicthCheck extends StatefulWidget {
  SwicthCheck({Key? key}) : super(key: key);

  @override
  createState() => _swicthCheck();
}

class _swicthCheck extends State<SwicthCheck> {
  bool _check = false;
  bool _switch = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        new Checkbox(
            value: _check,
            onChanged: (b) {
              setState(() {
                _check = !_check;
              });
            }),
        new Switch(
            value: _switch,
            onChanged: (c) {
              setState(() {
                _switch = c;
              });
            }),
        new Text(
          'WorldHello WorldHello World',
          style: new TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.w400,
              fontFamily: "Georgia",
              color: Colors.blue[400]),
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.right,
          // maxLines: 10,
        ),
      ],
    );
  }
}

class RandomWords extends StatefulWidget {
  RandomWords({Key? key}) : super(key: key);

  @override
  createState() => _nameState();
}

class _nameState extends State<RandomWords> {
  int count = 0;
  @override
  Widget build(BuildContext context) {
    // return Container();
    return new Column(children: [
      Text("state组件count:$count"),
      ElevatedButton(
          onPressed: () {
            setState(() {
              count++;
            });
          },
          child: Text('漂浮按钮')),
      TextButton(
          onPressed: () {
            setState(() {
              count++;
            });
          },
          child: Text('文字按钮')),
      OutlinedButton(
          onPressed: () {
            setState(() {
              count++;
            });
          },
          child: Text('边框按钮')),
      IconButton(
        onPressed: () {
          setState(() {
            count--;
          });
        },
        icon: new Icon(
          Icons.beach_access,
          color: Colors.blue[100],
          size: 36.0,
        ),
      ),
      ElevatedButton.icon(
          onPressed: () {
            setState(() {
              count--;
            });
          },
          label: Text('图标按钮'),
          icon: new Icon(
            Icons.beach_access,
          )),
      LinearProgressIndicator(
        value: .8,
        valueColor: AlwaysStoppedAnimation(Colors.red[700]),
      ),
      SizedBox(
        height: 16,
        // child: Text('SizedBox'),
      ),
      CupertinoActivityIndicator(),
      Container(
          child: CircularProgressIndicator(
            // value: .5,
            valueColor: AlwaysStoppedAnimation(Colors.green[400]),
          ),
          width: 100,
          height: 100),
      GestureDetector(
        onTap: () {
          print('点击事件');
        },
        onLongPress: () {
          print('长按点击事件');
        },
        child: Text('点我'),
      )
    ]);
  }
}
