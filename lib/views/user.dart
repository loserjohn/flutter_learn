/*
 * @Author: your name
 * @Date: 2021-11-22 15:25:16
 * @LastEditTime: 2022-01-20 16:00:22
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \flutter_application_1\lib\main.dart
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'page2.dart';
import 'page1.dart';

class Users extends StatelessWidget {
  Users({
    Key? key,
    @required id,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var args=ModalRoute.of(context).settings.arguments;
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('表单测试'), //标题
        elevation: 10.0, //下阴影
        centerTitle: true, //居中文字
      ),
      body: new SingleChildScrollView(
          child: new Center(
            child: new Container(
              child: new Column(
                children: [
                  new FormCheck(),
                  new SwicthCheck(),
                  new Text(
                    'WorldHello ',
                    style: new TextStyle(
                        fontSize: 24.0,
                        fontWeight: FontWeight.w400,
                        fontFamily: "Georgia",
                        color: Colors.blue[400]),
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.right,
                    // maxLines: 10,
                  ),
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, '回传的参数');
                      },
                      child: Text('back'))
                ],
              ),
              color: Colors.white,
              // height: 60,
            ),
          ),
          scrollDirection: Axis.vertical),
    );
  }
}
