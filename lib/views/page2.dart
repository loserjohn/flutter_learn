/*
 * @Author: your name
 * @Date: 2021-11-24 14:34:17
 * @LastEditTime: 2021-11-27 17:41:26
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \flutter_application_1\lib\views\page1.dart
 */

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FormCheck extends StatefulWidget {
  FormCheck({Key? key}) : super(key: key);

  @override
  _FormState createState() => _FormState();
}

class _FormState extends State<FormCheck> {
  // String a = '我是一段问字';
  GlobalKey _key = GlobalKey<FormState>();
  TextEditingController _user = TextEditingController();
  TextEditingController _pass = TextEditingController();
  @override
  Widget build(BuildContext context) {
    // return new Text("123$a");
    return Form(
        key: _key,
        child: Column(
          children: [
            TextFormField(
              controller: _user,
              autofocus: true,
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.ac_unit),
                  labelText: '用户名',
                  hintText: '请输入账号'),
            ),
            SizedBox(height: 40),
            TextFormField(
              controller: _pass,
              // textAlign: TextAlign.center,
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.ac_unit),
                  labelText: '密码',
                  hintText: '请输入密码123'),
              obscureText: true,
              textInputAction: TextInputAction.search,
              onFieldSubmitted: (v) {
                print('搜索');
              },
            ),
            SizedBox(height: 40),
            ElevatedButton(
                onPressed: () {
                  print(_pass.text);
                  print((_key.currentState as FormState).validate());
                },
                child: Text('提交')),
          ],
        ));
  }
}
