/*
 * @Author: your name
 * @Date: 2021-11-22 15:25:16
 * @LastEditTime: 2022-01-20 16:11:33
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \flutter_application_1\lib\main.dart
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Layload extends StatelessWidget {
  Layload({
    Key? key,
    @required id,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var args=ModalRoute.of(context).settings.arguments;
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('布局部分'), //标题
        elevation: 10.0, //下阴影
        centerTitle: true, //居中文字
      ),
      body: new SingleChildScrollView(
          child: Column(
            //测试Row对齐方式，排除Column默认居中对齐的干扰
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(" hello world "),
                  Text(" I am Jack "),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(" hello world "),
                  Text(" I am Jack "),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                textDirection: TextDirection.rtl,
                children: <Widget>[
                  Text(" hello world "),
                  Text(" I am Jack "),
                ],
              ),
              Wrap(
                spacing: 8.0, // 主轴(水平)方向间距
                runSpacing: 4.0, // 纵轴（垂直）方向间距
                alignment: WrapAlignment.center, //沿主轴方向居中
                children: <Widget>[
                  Chip(
                    avatar: CircleAvatar(
                        backgroundColor: Colors.blue, child: Text('A')),
                    label: Text('Hamilton'),
                  ),
                  Chip(
                    avatar: CircleAvatar(
                        backgroundColor: Colors.blue, child: Text('M')),
                    label: Text('Lafayette'),
                  ),
                  Chip(
                    avatar: CircleAvatar(
                        backgroundColor: Colors.blue, child: Text('H')),
                    label: Text('Mulligan'),
                  ),
                  Chip(
                    avatar: CircleAvatar(
                        backgroundColor: Colors.blue, child: Text('J')),
                    label: Text('Laurens'),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  //Flex的两个子widget按1：2来占据水平空间
                  Flex(
                    direction: Axis.horizontal,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Container(
                          height: 30.0,
                          color: Colors.red,
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Container(
                          height: 30.0,
                          color: Colors.green,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: SizedBox(
                      height: 60.0,
                      //Flex的三个子widget，在垂直方向按2：1：1来占用100像素的空间
                      child: Flex(
                        direction: Axis.vertical,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Container(
                              height: 10.0,
                              width: 200,
                              color: Colors.red,
                              child: Text("的大范甘迪发鬼地方给对方三个地方房贷公司发的广泛地光伏给对方谁发的"),
                            ),
                          ),
                          Spacer(
                            flex: 3,
                          ),
                          Expanded(
                            flex: 1,
                            child: Container(
                              height: 30.0,
                              color: Colors.green,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Stack(
                    alignment: Alignment.center,
                    // fit: StackFit.expand, //未定位widget占满Stack整个空间
                    children: <Widget>[
                      Positioned(
                        left: 18.0,
                        child: Text("I am Jack",
                            style: TextStyle(color: Colors.black)),
                      ),
                      Container(
                        child: Text("Hello world",
                            style: TextStyle(color: Colors.white)),
                        color: Colors.red,
                        width: 200,
                        height: 200,
                      ),
                      Positioned(
                        right: 0,
                        child: Text("Your friend",
                            style: TextStyle(color: Colors.black)),
                        bottom: 0,
                      )
                    ],
                  ),
                  Container(
                    height: 120.0,
                    width: 120.0,
                    color: Colors.red[100],
                    child: Align(
                      widthFactor: 2,
                      heightFactor: 2,
                      alignment: Alignment.topRight,
                      child: FlutterLogo(
                        size: 60,
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
          scrollDirection: Axis.vertical),
    );
  }
}
