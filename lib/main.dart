/*
 * @Author: your name
 * @Date: 2021-11-22 15:25:16
 * @LastEditTime: 2022-01-21 16:58:23
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \flutter_application_1\lib\main.dart
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// import './pages/user.dart';
import './views/layout.dart';
import './views/contain.dart';
import './views/scroll.dart';

import './pages/tabsBar.dart';
import './pages/index.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: 'Welcome to Flutter',
        //路由配置
        routes: {
          '/': (context) => Index(),
          // '/users': (context) => Users(),
          '/layout': (context) => Layload(),
          '/contain': (context) => Contain(),
          '/scroll': (context) => Scroll(),
        },
        theme: new ThemeData(
          primarySwatch: Colors.yellow, // 设置主题颜色
        ),
        // home: Home(),
        initialRoute: "/",
        onGenerateRoute: (RouteSettings settings) {
          print(settings);
        });
  }
}
